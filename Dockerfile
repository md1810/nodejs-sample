FROM node:16.4-alpine
ENV NODE_ENV=production

WORKDIR /app
EXPOSE 3004
COPY ["package.json", "package-lock.json*", "./"]

RUN npm install --production

COPY . .

CMD [ "node", "index.js" ]
