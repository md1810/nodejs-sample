const http = require('http');
const port = process.env.PORT || 3004;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  const msg = 'Hello World! New from MD 3rd final \n'
  res.end(msg);
});

server.listen(port, () => {
  console.log(`Server running on http://localhost:${port}/`);
});
